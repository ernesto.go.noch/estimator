package com.taxigoexpress.tgestimator.data.repositoryImplementatios

import com.taxigoexpress.tgestimator.data.datasourceImplementations.TripsDatasourceImpl
import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.tgestimator.domain.repositoryAbstractions.TripsRepository
import io.reactivex.Observable
import javax.inject.Inject

class TripsRepositoryImpl
@Inject constructor(private val tripsDatasourceImpl: TripsDatasourceImpl) : TripsRepository {

    override fun calculatePrice(
        request: TripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse> =
        tripsDatasourceImpl.calculatePrices(request,serviceType)


    override fun getTripInformation(
        origin: String, destination: String
    ): Observable<TripsInformationResponse> =
        tripsDatasourceImpl.getTripInformation(origin, destination)


}