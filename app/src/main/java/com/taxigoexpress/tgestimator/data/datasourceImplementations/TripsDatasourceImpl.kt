package com.taxigoexpress.tgestimator.data.datasourceImplementations

import com.taxigoexpress.tgestimator.data.services.MapsServices
import com.taxigoexpress.tgestimator.data.services.TripServices
import com.taxigoexpress.tgestimator.domain.datasourceAbstractions.TripsDatasource
import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable
import javax.inject.Inject

class TripsDatasourceImpl
@Inject constructor(
    private val tripServices: TripServices,
    private val mapsServices: MapsServices
) : TripsDatasource {
    override fun calculatePrices(
        request: TripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse> =
        tripServices.calculatePrice(
            serviceType.type.toString(),
            request
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(
                    PriceResponse(
                        serviceType.type,
                        it.body()
                    )
                )
            } else {
                Observable.error(Throwable(it.message()))
            }
        }

    override fun getTripInformation(
        origin: String,
        destination: String
    ): Observable<TripsInformationResponse> =
        this.mapsServices.getTripInformation(
            origin,
            destination
        ).flatMap {
            if (it.isSuccessful) {
                Observable.just(it.body())
            } else {
                Observable.error(Throwable(it.message()))
            }
        }


}
