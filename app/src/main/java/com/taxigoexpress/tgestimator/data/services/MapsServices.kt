package com.taxigoexpress.tgestimator.data.services

import com.google.android.gms.common.internal.safeparcel.SafeParcelable
import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MapsServices {

    @GET(BuildConfig.GET_TRIP_INFORMATION)
    fun getTripInformation(
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("sensor") sensor: Boolean = false,
        @Query("mode") mode: String = "driving"
    ): Observable<Response<TripsInformationResponse>>
}