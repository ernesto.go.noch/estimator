package com.taxigoexpress.tgestimator.data.services

import com.taxigoexpress.core.BuildConfig
import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

interface TripServices {

    @PUT(BuildConfig.POST_TRIP)
    fun calculatePrice(
        @Path("serviceType") type: String,
        @Body tripRequest: TripRequest
    ): Observable<Response<String>>
}
