package com.taxigoexpress.tgestimator.presentation.utils

import com.google.android.gms.maps.model.LatLng
import java.text.DecimalFormat


object Utilities {

    const val LEGS = "legs"
    const val DISTANCE = "distance"
    const val DURATION = "duration"
    const val STEPS = "steps"
    const val TEXT = "text"
    const val VALUE = "value"
    const val ROUTES = "routes"
    const val POLYLINE = "polyline"
    const val RESULTS = "results"
    const val ADDRESS = "formatted_address"
    const val PLACE_ID = "place_id"
    private const val APIKEY = "AIzaSyAmlXHQc-tlACdOFcP8v1EkOh-yjlnn7Rk"
    private const val URL_DIRECTIONS = "https://maps.googleapis.com/maps/api/directions/"
    private const val URL_GEOCODE = "https://maps.googleapis.com/maps/api/geocode/"
    private const val DECIMAL_FORMAT = "#0.00"

    fun getDirectionsUrl(
        origin: LatLng,
        dest: LatLng
    ): String? { // Origin of route
        val strOrigin = "origin=" + origin.latitude + "," + origin.longitude
        // Destination of route
        val strDest = "destination=" + dest.latitude + "," + dest.longitude
        // Sensor enabled
        val sensor = "sensor=false"
        val mode = "mode=driving"
        // Building the parameters to the web service
        val parameters = "$strOrigin&$strDest&$sensor&$mode"
        // Output format
        val output = "json?key="
        // Building the url to the web service
        val apikey = output + APIKEY
        return "$URL_DIRECTIONS$apikey&$parameters"
    }

    fun getGeocodeUrl(
        latlng: LatLng
    ): String? { // Origin of route
        val strParams = "latlng=" + latlng.latitude + "," + latlng.longitude
        // Destination of route
        // Sensor enabled
        val sensor = "sensor=false"
        // Building the parameters to the web service
        val parameters = "$strParams&$sensor"
        // Output format
        val output = "json?key="
        // Building the url to the web service
        val apikey = output + APIKEY
        return "$URL_GEOCODE$apikey&$parameters"
    }

    /**
     * Method to decode polyline points
     * Courtesy : https://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
     */
    fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }

        return poly
    }

    fun getCostFormat(cost: Double?): String =
        String.format(
            "$ %s", DecimalFormat(DECIMAL_FORMAT)
                .format(cost)
        )

    /*fun calculateCost(
        response: TripsInformationResponse,
        duration: Int,
        distance: Int
    ): Double {
        val finalDistance = (distance.toDouble() / 1000)
        val finalDuration = duration.toDouble() / 60
        var total = (finalDistance * response.costKm) + (finalDuration * response.costMinute)
        if (finalDistance <= response.minimumDistance) {
            total += response.minimumFee
        }
        */
    /**
     * Cuota de solicitud $20.00
     * el banderazo solo aplica los primeros 7 kms
     * $7 pesos por km
     * $0.50 pesos el minuto
     *//*
        return total
    }*/


}