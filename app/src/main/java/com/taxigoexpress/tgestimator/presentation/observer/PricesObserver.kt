package com.taxigoexpress.tgestimator.presentation.observer

import android.view.View
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.tgestimator.R
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import java.io.IOException

class PricesObserver(private val mTripsLiveData: BaseLiveData<List<PriceResponse>>) :
    DefaultObserverLiveData<List<PriceResponse>>(mTripsLiveData) {

    override fun onStart() {
        this.mTripsLiveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mTripsLiveData.loadingObserver.value = View.GONE
    }

    override fun onNext(result: List<PriceResponse>) {
        this.mTripsLiveData.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mTripsLiveData.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mTripsLiveData.showErrorObserver.value =
                R.string.no_internet_connection
            else -> this.mTripsLiveData.showExceptionObserver.value =
                error.localizedMessage
        }
    }


}
