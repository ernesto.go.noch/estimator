package com.taxigoexpress.tgestimator.presentation.viewmodel.abstraction

import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.PresenterLiveData
import com.taxigoexpress.places.model.PlaceDetails
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse

interface TripsViewModel<T> : PresenterLiveData<T> {
    fun calculatePrice(request: TripsInformationResponse.LegsResponse)
    fun observablePrice(): BaseLiveData<T>
    fun getTripInformation(params:Pair<PlaceDetails, PlaceDetails>)
    fun observableTripInformation(): BaseLiveData<TripsInformationResponse>
}