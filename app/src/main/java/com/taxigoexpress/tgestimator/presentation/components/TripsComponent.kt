package com.taxigoexpress.tgestimator.presentation.components

import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.tgestimator.presentation.fragments.MainFragment
import com.taxigoexpress.tgestimator.presentation.modules.TripsModule
import dagger.Component

@FragmentScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [TripsModule::class]
)
interface TripsComponent {
    fun inject(mainFragment: MainFragment)
}