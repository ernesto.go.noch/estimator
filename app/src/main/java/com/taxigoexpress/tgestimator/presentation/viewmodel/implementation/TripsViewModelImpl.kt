package com.taxigoexpress.tgestimator.presentation.viewmodel.implementation

import androidx.lifecycle.MutableLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.core.presentation.viewmodel.BaseViewModelLiveData
import com.taxigoexpress.places.model.PlaceDetails
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.tgestimator.domain.useCases.CalculatePriceUseCase
import com.taxigoexpress.tgestimator.domain.useCases.GetTripInformationUseCase
import com.taxigoexpress.tgestimator.presentation.observer.PricesObserver
import com.taxigoexpress.tgestimator.presentation.observer.TripConstantsObserver
import com.taxigoexpress.tgestimator.presentation.viewmodel.abstraction.TripsViewModel
import javax.inject.Inject

class TripsViewModelImpl
@Inject constructor(
    private val calculatePriceUseCase: CalculatePriceUseCase,
    private val getTripInformationUseCase: GetTripInformationUseCase
) : BaseViewModelLiveData<List<PriceResponse>>(), TripsViewModel<List<PriceResponse>> {

    private val mTripsLiveData by lazy { this.getCustomLiveData() }

    private val mTripInformationLiveData by lazy {
        this.buildLiveData(
            MutableLiveData<TripsInformationResponse>()
        )
    }

    override fun calculatePrice(request: TripsInformationResponse.LegsResponse) {
        this.calculatePriceUseCase.execute(
            PricesObserver(this.mTripsLiveData),
            request
        )
    }

    override fun observablePrice(): BaseLiveData<List<PriceResponse>> =
        this.mTripsLiveData

    override fun observeResult(): BaseLiveData<List<PriceResponse>> = this.mTripsLiveData

    override fun getTripInformation(params: Pair<PlaceDetails, PlaceDetails>) {
        this.getTripInformationUseCase.execute(
            TripConstantsObserver(this.mTripInformationLiveData),
            params
        )
    }

    override fun observableTripInformation(): BaseLiveData<TripsInformationResponse> =
        this.mTripInformationLiveData

    override fun onCleared() {
        this.calculatePriceUseCase.dispose()
        this.getTripInformationUseCase.dispose()
        super.onCleared()
    }

}