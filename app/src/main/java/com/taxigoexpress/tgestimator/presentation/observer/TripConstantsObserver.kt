package com.taxigoexpress.tgestimator.presentation.observer

import android.view.View
import com.taxigoexpress.core.presentation.observers.DefaultObserverLiveData
import com.taxigoexpress.core.presentation.view.BaseLiveData
import com.taxigoexpress.tgestimator.R
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import java.io.IOException

class TripConstantsObserver(private val mTripInformationLiveData: BaseLiveData<TripsInformationResponse>) :
    DefaultObserverLiveData<TripsInformationResponse>(mTripInformationLiveData) {

    override fun onStart() {
        this.mTripInformationLiveData.loadingObserver.value = View.VISIBLE
        super.onStart()
    }

    override fun onComplete() {
        this.mTripInformationLiveData.loadingObserver.value = View.GONE
    }

    override fun onNext(result: TripsInformationResponse) {
        this.mTripInformationLiveData.customObserver.value = result
    }

    override fun onError(error: Throwable) {
        this.mTripInformationLiveData.loadingObserver.value = View.GONE
        when (error) {
            is IOException -> this.mTripInformationLiveData.showErrorObserver.value =
                R.string.no_internet_connection
            else -> this.mTripInformationLiveData.showExceptionObserver.value =
                error.localizedMessage
        }
    }


}
