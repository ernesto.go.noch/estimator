package com.taxigoexpress.tgestimator.presentation.modules

import com.taxigoexpress.core.presentation.scopes.FragmentScope
import com.taxigoexpress.tgestimator.data.repositoryImplementatios.TripsRepositoryImpl
import com.taxigoexpress.tgestimator.data.services.MapsServices
import com.taxigoexpress.tgestimator.data.services.TripServices
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.repositoryAbstractions.TripsRepository
import com.taxigoexpress.tgestimator.presentation.viewmodel.abstraction.TripsViewModel
import com.taxigoexpress.tgestimator.presentation.viewmodel.implementation.TripsViewModelImpl
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
class TripsModule {

    @Provides
    @FragmentScope
    fun providesTripServices(@Named("main_retrofit") retrofit: Retrofit): TripServices =
        retrofit.create(TripServices::class.java)

    @Provides
    @FragmentScope
    fun providesMapsServices(@Named("google_maps_retrofit") retrofit: Retrofit): MapsServices =
        retrofit.create(MapsServices::class.java)

    @Provides
    @FragmentScope
    fun providesTripsRepository(tripsRepositoryImpl: TripsRepositoryImpl): TripsRepository =
        tripsRepositoryImpl

    @Provides
    @FragmentScope
    fun providesTripsViewModel(tripsViewModelImpl: TripsViewModelImpl): TripsViewModel<List<PriceResponse>> =
        tripsViewModelImpl
}