package com.taxigoexpress.tgestimator.presentation.activities

import android.os.Bundle
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.tgestimator.R
import com.taxigoexpress.tgestimator.presentation.fragments.MainFragment
import io.reactivex.plugins.RxJavaPlugins

class MainActivity : BaseActivity() {

    override fun getLayoutResId(): Int =
        R.layout.activity_main

    override fun initView(savedInstanceState: Bundle?) {
        this.replaceFragment(
            MainFragment(),
            R.id.main_container,
            false
        )

        RxJavaPlugins.setErrorHandler {  }

    }
}
