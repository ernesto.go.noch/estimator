package com.taxigoexpress.tgestimator.presentation.fragments


import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.res.Resources
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.beust.klaxon.array
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.ia.mchaveza.kotlin_library.TrackingManagerLocationCallback
import com.taxigoexpress.core.getApplicationComponent
import com.taxigoexpress.core.presentation.fragments.BaseFragment
import com.taxigoexpress.core.presentation.fragments.LoaderFragment
import com.taxigoexpress.core.presentation.utils.clickEvent
import com.taxigoexpress.core.presentation.utils.hideKeyboard
import com.taxigoexpress.places.PlaceAPI
import com.taxigoexpress.places.PlaceAPI.Companion.TEST_API_KEY
import com.taxigoexpress.places.adapter.PlacesAutoCompleteAdapter
import com.taxigoexpress.places.listener.OnPlacesDetailsListener
import com.taxigoexpress.places.model.Place
import com.taxigoexpress.places.model.PlaceDetails
import com.taxigoexpress.tgestimator.R
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.tgestimator.presentation.components.DaggerTripsComponent
import com.taxigoexpress.tgestimator.presentation.components.TripsComponent
import com.taxigoexpress.tgestimator.presentation.modules.TripsModule
import com.taxigoexpress.tgestimator.presentation.utils.Utilities
import com.taxigoexpress.tgestimator.presentation.utils.Utilities.ADDRESS
import com.taxigoexpress.tgestimator.presentation.utils.Utilities.PLACE_ID
import com.taxigoexpress.tgestimator.presentation.utils.Utilities.RESULTS
import com.taxigoexpress.tgestimator.presentation.utils.Utilities.decodePoly
import com.taxigoexpress.tgestimator.presentation.utils.Utilities.getGeocodeUrl
import com.taxigoexpress.tgestimator.presentation.viewmodel.abstraction.TripsViewModel
import kotlinx.android.synthetic.main.bottom_sheet_main.*
import kotlinx.android.synthetic.main.main_view_layout.*
import kotlinx.android.synthetic.main.search_view_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.onComplete
import org.jetbrains.anko.uiThread
import java.net.URL
import javax.inject.Inject
import kotlin.math.absoluteValue


/**
 * A simple [Fragment] subclass.
 */
class MainFragment : BaseFragment(),
    OnMapReadyCallback,
    PermissionCallback,
    TrackingManagerLocationCallback {

    private var currentAddress: String? = null
    private var bottomSheetB: BottomSheetBehavior<ConstraintLayout>? = null
    @Inject
    lateinit var tripsViewModel: TripsViewModel<List<PriceResponse>>

    private var originPlaceId: String? = null
    private var destinationPlaceId: String? = null
    private var mGoogleMap: GoogleMap? = null
    private var currentLocationFounded: Location? = null

    private val trackingManager: TrackingManager by lazy {
        TrackingManager(this.context!!)
    }

    private val permissionManager by lazy { PermissionManager(activity!!, this) }


    private val tripsComponent: TripsComponent by lazy {
        DaggerTripsComponent.builder()
            .applicationComponent(activity?.getApplicationComponent())
            .tripsModule(TripsModule())
            .build()
    }

    private val loaderDialog by lazy { LoaderFragment.newInstance(getString(R.string.loading)) }

    override fun getResourceLayout(): Int = R.layout.fragment_main_layout

    override fun initView(view: View, savedInstanceState: Bundle?) {
        this.tripsComponent.inject(this)
        this.mvMainTrip.onCreate(savedInstanceState)
        this.mvMainTrip.getMapAsync(this)
        this.observeResults()
        this.setClickListeners()
    }

    private fun observeResults() {

        with(this.tripsViewModel.observableTripInformation()) {
            this.customObserver.observe(this@MainFragment, Observer {
                it?.let {
                    paintMapRoute(it)
                }
            })

            this.loadingObserver.observe(this@MainFragment, Observer {
                if (it == View.GONE) {
                    this@MainFragment.loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@MainFragment, Observer {
                this@MainFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@MainFragment, Observer {
                this@MainFragment.showAlertWithResource(it ?: 0)
            })
        }

        with(this.tripsViewModel.observablePrice()) {
            this.customObserver.observe(this@MainFragment, Observer {
                it?.let {
                    showTripCost(it)
                }
            })

            this.loadingObserver.observe(this@MainFragment, Observer {
                if (it == View.GONE) {
                    this@MainFragment.loaderDialog.dismiss()
                }
            })

            this.showExceptionObserver.observe(this@MainFragment, Observer {
                this@MainFragment.showAlert(it ?: "")
            })

            this.showErrorObserver.observe(this@MainFragment, Observer {
                this@MainFragment.showAlertWithResource(it ?: 0)
            })
        }

    }

    private fun showTripCost(prices: List<PriceResponse>) {

        prices.forEach {
            when (it.serviceType) {
                PriceResponse.ServiceTypeEnum.Basic -> this.tvPriceBasic.text = Utilities.getCostFormat(it._price?.toDouble())
                PriceResponse.ServiceTypeEnum.Premium ->  this.tvPricePremium.text = Utilities.getCostFormat(it._price?.toDouble())
                else -> this.tvPriceDeluxe.text = Utilities.getCostFormat(it._price?.toDouble())
            }
        }

        this.bottomSheetB?.state = BottomSheetBehavior.STATE_EXPANDED
        this.ivCloseBottomSheet.clickEvent().observe(this,
            Observer {
                bottomSheetB?.state = BottomSheetBehavior.STATE_HIDDEN
            })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        this.mvMainTrip.onSaveInstanceState(outState)
    }

    private fun setClickListeners() {
        this.bottomSheetB = BottomSheetBehavior.from(bottomSheet)
        this.bottomSheetB?.state = BottomSheetBehavior.STATE_HIDDEN
        this.actDestination.hint = this.getString(R.string.pick_destination)
        this.actOrigin.hint = this.getString(R.string.pick_origin)

        val placesApi = this.activity?.let {
            PlaceAPI.Builder()
                .apiKey(TEST_API_KEY)
                .build(it)
        }

        placesApi?.let { api ->
            this.actOrigin.setAdapter(this.context?.let { PlacesAutoCompleteAdapter(it, api) })
            this.actDestination.setAdapter(this.context?.let { PlacesAutoCompleteAdapter(it, api) })
        }

        this.actOrigin.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as Place
                this.originPlaceId = place.id
                this.actOrigin.setText(place.description)
                this.onSelectedPlace()
                this.checkButtonAvailability()
            }

        this.actDestination.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                val place = parent.getItemAtPosition(position) as Place
                this.destinationPlaceId = place.id
                this.actDestination.setText(place.description)
                this.onSelectedPlace()
                this.checkButtonAvailability()
            }

        this.btnCheckTrip.clickEvent().observe(this, Observer {
            this.loaderDialog.show(
                childFragmentManager,
                MainFragment::class.java.name
            )
            this.originPlaceId?.let { origin ->
                placesApi?.fetchPlaceDetails(origin, object : OnPlacesDetailsListener {
                    override fun onPlaceDetailsFetched(originPlaceDetails: PlaceDetails) {
                        destinationPlaceId?.let { destination ->
                            placesApi.fetchPlaceDetails(destination,
                                object : OnPlacesDetailsListener {
                                    override fun onPlaceDetailsFetched(destinationPlaceDetails: PlaceDetails) {
                                        this@MainFragment.activity?.runOnUiThread {
                                            this@MainFragment.tripsViewModel.getTripInformation(
                                                Pair(
                                                    originPlaceDetails,
                                                    destinationPlaceDetails
                                                )
                                            )
                                        }
                                    }

                                    override fun onError(errorMessage: String) {
                                        this@MainFragment.showAlert(errorMessage)
                                        this@MainFragment.loaderDialog.dismiss()
                                    }
                                })
                        }
                    }

                    override fun onError(errorMessage: String) {
                        this@MainFragment.showAlert(errorMessage)
                        this@MainFragment.loaderDialog.dismiss()
                    }

                })
            }
        })

        this.tvUseCurrentLocation.clickEvent().observe(this, Observer {
            this.swUseCurrentLocation.isEnabled = !this.swUseCurrentLocation.isEnabled
        })

        this.swUseCurrentLocation.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                this.currentLocationFounded?.let { this.getCurrentDirection(it) }
            } else {
                this.actOrigin.isEnabled = true
            }
        }
    }

    private fun paintMapRoute(
        tripsInformationResponse: TripsInformationResponse
    ) {
        this.mGoogleMap?.clear()
        val latLongB = LatLngBounds.Builder()
        val origin = tripsInformationResponse._routes?.first()?._legs?.first()?._startLocation!!
        val destination = tripsInformationResponse._routes.first()._legs?.first()?._endLocation!!
        val originPlace = origin._lat?.let { origin._lng?.let { lng -> LatLng(it, lng) } }
        val destinationPlace = destination._lat?.let {
            destination._lng?.let { lng ->
                LatLng(
                    it,
                    lng
                )
            }
        }
        val originMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_start_pin)
        val destinationMarker = BitmapDescriptorFactory.fromResource(R.drawable.ic_end_pin)
        this.mGoogleMap!!.addMarker(
            originPlace?.let { MarkerOptions().position(it).icon(originMarker) })
        this.mGoogleMap!!.addMarker(
            destinationPlace?.let { MarkerOptions().position(it).icon(destinationMarker) })
        // Declare polyline object and set up color and width
        val options = PolylineOptions()
        activity?.applicationContext?.let { context ->
            ContextCompat.getColor(context, R.color.colorPrimary)
        }?.let { color ->
            options.color(color)
        }
        options.width(10f)
        val points = tripsInformationResponse._routes.first()._legs?.first()?._steps
        // For every element in the JsonArray, decode the polyline string and pass all points to a List
        val polypts = points!!.flatMap { decodePoly(it._polyline?._points!!) }

        // Add  points to polyline and bounds
        options.add(originPlace)
        latLongB.include(originPlace)
        for (point in polypts) {
            options.add(point)
            latLongB.include(point)
        }
        options.add(destinationPlace)
        latLongB.include(destinationPlace)
        // build bounds
        val bounds = latLongB.build()
        // add polyline to the map
        mGoogleMap?.addPolyline(options)
        // show map with route centered
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 120))

        tvDistance.text =
            tripsInformationResponse._routes.first()._legs?.first()?._distance?._text
        tvEstimatedTime.text =
            tripsInformationResponse._routes.first()._legs?.first()?._duration?._text

        tripsInformationResponse._routes.first()._legs?.let {
            this.tripsViewModel.calculatePrice(it.first())
        }
    }


    private fun getCurrentDirection(currentLocationFounded: Location) {
        this.loaderDialog.show(
            childFragmentManager,
            MainFragment::class.java.name
        )
        val url = getGeocodeUrl(
            LatLng(
                currentLocationFounded.latitude,
                currentLocationFounded.longitude
            )
        )
        doAsync {
            val result = URL(url).readText()
            uiThread {
                val parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                // get to the correct element in JsonObject
                val results = json.array<JsonObject>(RESULTS)
                currentAddress = results?.get(0)?.get(ADDRESS) as String
                originPlaceId = results[0][PLACE_ID] as String
            }
            onComplete {
                loaderDialog.dismiss()
                currentAddress?.let { address -> setCurrentLocationAsOrigin(address) }
            }
        }
    }

    private fun setCurrentLocationAsOrigin(currentAddress: String) {
        if (currentAddress.isNotEmpty()) {
            actOrigin.isEnabled = false
            actOrigin.setText(currentAddress)
        }
    }

    private fun onSelectedPlace() {
        this.hideKeyboard()
    }

    private fun checkButtonAvailability() {
        this.originPlaceId?.let { origin ->
            this.destinationPlaceId?.let { destiny ->
                if (origin != destiny) {
                    this.btnCheckTrip.isEnabled = true
                }
            }
        }
    }


    override fun onMapReady(p0: GoogleMap?) {
//        p0?.addMarker(MarkerOptions().position(LatLng(0.0, 0.0)).title("Marker"))
        this.mGoogleMap = p0
        this.setCurrentLocationOnMap()
        this.changeMapTheme()
    }

    private fun changeMapTheme() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            this.mGoogleMap?.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    this.context, R.raw.x_gmap_style_variant1
                )
            )
        } catch (e: Resources.NotFoundException) {
            Log.e(ContentValues.TAG, "Can't find style. Error: ", e)
        }

    }

    private fun setCurrentLocationOnMap() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!this.permissionManager.requestSinglePermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        } else {
            if (permissionManager.permissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
                this.startLocating()
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocating() {
        this.mGoogleMap?.isMyLocationEnabled = true
        this.mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = true
//        if (this.trackingManager.areLocationServicesEnabled()) {
        this.trackingManager.startLocationUpdates(
            this,
            2000,
            1000
        )
//        }
    }

    override fun onLocationHasChanged(location: Location) {
        this.currentLocationFounded?.let {
            val diffLAtitude = (it.latitude - location.latitude).absoluteValue
            val diffLongitude = (it.longitude - location.longitude).absoluteValue
            if (diffLAtitude >= 10 || diffLongitude >= 10) {
                this.addMarkerToMap(location)
            }
        } ?: kotlin.run {
            this.addMarkerToMap(location)
        }
        this.currentLocationFounded = location
    }

    override fun onLocationHasChangedError(error: Exception) {
        this.trackingManager.stopLocationUpdates()
    }

    private fun addMarkerToMap(location: Location) {
        this.mGoogleMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(location.latitude, location.longitude),
                18f
            )
        )
        this.mGoogleMap?.animateCamera(CameraUpdateFactory.zoomTo(15f))
    }

    override fun onPermissionDenied(permission: String) {
    }

    override fun onPermissionGranted(permission: String) {
        this.setCurrentLocationOnMap()
    }

    override fun onResume() {
        super.onResume()
        this.mvMainTrip?.onResume()
    }

    override fun onPause() {
        super.onPause()
        this.mvMainTrip?.onPause()
    }

    override fun onStart() {
        super.onStart()
        this.mvMainTrip?.onStart()
    }

    override fun onStop() {
        super.onStop()
        this.mvMainTrip?.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.mvMainTrip?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        this.mvMainTrip?.onLowMemory()
    }


}
