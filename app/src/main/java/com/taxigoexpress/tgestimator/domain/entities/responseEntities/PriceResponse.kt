package com.taxigoexpress.tgestimator.domain.entities.responseEntities

data class PriceResponse(

    val _serviceType: Int? = null,
    val _price: String? = null

) {

    val serviceType: ServiceTypeEnum
        get() {
            return when (_serviceType) {
                1 -> ServiceTypeEnum.Basic
                2 -> ServiceTypeEnum.Premium
                else -> ServiceTypeEnum.Deluxe
            }
        }

    enum class ServiceTypeEnum(val type: Int) {
        Basic(1),
        Premium(2),
        Deluxe(3)
    }
}
