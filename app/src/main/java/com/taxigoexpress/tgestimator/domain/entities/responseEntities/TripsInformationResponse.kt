package com.taxigoexpress.tgestimator.domain.entities.responseEntities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.taxigoexpress.tgestimator.presentation.utils.Utilities

data class TripsInformationResponse(
    @Expose
    @SerializedName(Utilities.ROUTES)
    val _routes: List<RoutesResponse>? = null
) {
    data class RoutesResponse(
        @Expose
        @SerializedName(Utilities.LEGS)
        val _legs: List<LegsResponse>? = null
    )

    data class LegsResponse(
        @Expose
        @SerializedName(Utilities.DISTANCE)
        val _distance: BaseResponse? = null,

        @Expose
        @SerializedName(Utilities.DURATION)
        val _duration: BaseResponse? = null,

        @Expose
        @SerializedName(Utilities.STEPS)
        val _steps: List<StepResponse>? = null,

        @Expose
        @SerializedName("end_location")
        val _endLocation: StepResponse.LatLngResponse? = null,

        @Expose
        @SerializedName("start_location")
        val _startLocation: StepResponse.LatLngResponse? = null
    ) {
        data class BaseResponse(
            @Expose
            @SerializedName(Utilities.TEXT)
            val _text: String? = null,

            @Expose
            @SerializedName(Utilities.VALUE)
            val _value: Double? = null
        )

        data class StepResponse(
            @Expose
            @SerializedName(Utilities.DISTANCE)
            val _distance: BaseResponse? = null,

            @Expose
            @SerializedName(Utilities.DURATION)
            val _duration: BaseResponse? = null,

            @Expose
            @SerializedName("polyline")
            val _polyline: PolylineResponse? = null


        ) {
            data class LatLngResponse(
                @Expose
                @SerializedName("lat")
                val _lat: Double? = null,

                @Expose
                @SerializedName("lng")
                val _lng: Double? = null
            )

            data class PolylineResponse(
                @Expose
                @SerializedName("points")
                val _points: String? = null
            )
        }
    }
}
