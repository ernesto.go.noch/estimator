package com.taxigoexpress.tgestimator.domain.useCases

import com.taxigoexpress.core.domain.executors.PostExecutionThread
import com.taxigoexpress.core.domain.executors.ThreadExecutor
import com.taxigoexpress.core.domain.useCases.UseCase
import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import com.taxigoexpress.tgestimator.domain.repositoryAbstractions.TripsRepository
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CalculatePriceUseCase
@Inject constructor(
    private val tripsRepository: TripsRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : UseCase<List<PriceResponse>, TripsInformationResponse.LegsResponse>(
    threadExecutor,
    postExecutionThread
) {

    override fun createObservable(params: TripsInformationResponse.LegsResponse):
            Observable<List<PriceResponse>> =
        Observable.zip(
            tripsRepository.calculatePrice(
                generateRequest(params),
                PriceResponse.ServiceTypeEnum.Basic
            ).subscribeOn(Schedulers.newThread()),
            tripsRepository.calculatePrice(
                generateRequest(params),
                PriceResponse.ServiceTypeEnum.Premium
            ).subscribeOn(Schedulers.newThread()),
            tripsRepository.calculatePrice(
                generateRequest(params),
                PriceResponse.ServiceTypeEnum.Deluxe
            ).subscribeOn(Schedulers.newThread()),
            Function3 { basic, premium, deluxe ->
                arrayListOf(basic, premium, deluxe)
            })


    private fun generateRequest(params: TripsInformationResponse.LegsResponse): TripRequest {
        val origin = params._startLocation
        val destination = params._endLocation
        val time = params._duration!!._value!! / 60
        return TripRequest(
            origin?._lat,
            origin?._lng,
            destination?._lat,
            destination?._lng,
            time
        )
    }


}