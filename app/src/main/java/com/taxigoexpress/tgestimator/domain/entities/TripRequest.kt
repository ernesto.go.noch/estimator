package com.taxigoexpress.tgestimator.domain.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TripRequest(

    @Expose
    @SerializedName("latitudOrigen")
    private val _originLatitude: Double? = null,

    @Expose
    @SerializedName("longitudOrigen")
    private val _originLongitude: Double? = null,

    @Expose
    @SerializedName("latitudDestino")
    private val _destinationLatitude: Double? = null,

    @Expose
    @SerializedName("longitudDestino")
    private val _destinationLongitude: Double? = null,

    @Expose
    @SerializedName("tiempo")
    private val _time: Double? = null

) {


}
