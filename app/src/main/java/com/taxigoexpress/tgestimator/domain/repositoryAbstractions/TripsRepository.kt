package com.taxigoexpress.tgestimator.domain.repositoryAbstractions

import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable

interface TripsRepository {
    fun calculatePrice(
        request: TripRequest,
        serviceTypeEnum: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse>

    fun getTripInformation(
        origin: String, destination: String
    ): Observable<TripsInformationResponse>
}
