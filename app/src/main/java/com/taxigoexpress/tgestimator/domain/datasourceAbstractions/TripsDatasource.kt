package com.taxigoexpress.tgestimator.domain.datasourceAbstractions

import com.taxigoexpress.tgestimator.domain.entities.TripRequest
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.PriceResponse
import com.taxigoexpress.tgestimator.domain.entities.responseEntities.TripsInformationResponse
import io.reactivex.Observable

interface TripsDatasource {
    fun calculatePrices(
        request: TripRequest,
        serviceType: PriceResponse.ServiceTypeEnum
    ): Observable<PriceResponse>
    fun getTripInformation(
        origin: String, destination: String
    ): Observable<TripsInformationResponse>
}
