package com.taxigoexpress.core

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.taxigoexpress.core.presentation.components.ApplicationComponent
import com.taxigoexpress.core.presentation.components.DaggerApplicationComponent
import com.taxigoexpress.core.presentation.modules.ApplicationModule
import com.taxigoexpress.core.presentation.modules.NetModule

class TaxiGoExpressApplication : MultiDexApplication() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .netModule(
                NetModule(
                    this.getString(R.string.base_url),
                    this.getString(R.string.google_maps_url)
                )
            )
            .applicationModule(ApplicationModule(this))
            .build()
    }
}

/**
 * @return The application component from any class that have access or extends the Context class
 */
fun Context.getApplicationComponent(): ApplicationComponent =
    (this.applicationContext as TaxiGoExpressApplication).applicationComponent
