package com.taxigoexpress.core.domain.executors

import java.util.concurrent.Executor

interface ThreadExecutor : Executor