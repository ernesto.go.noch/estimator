package com.taxigoexpress.core.presentation.utils

object Constants {
    const val PASS_ID = "tge.pass.id"
    const val USER = "tge.user"
    const val IS_LOGGED = "is.logged"
    const val LOADING_MESSAGE = "loading.message"
    const val CARD_SCAN_REQUEST_CODE = 1076

}