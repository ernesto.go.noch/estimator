package com.taxigoexpress.core.presentation.fragments

import android.content.DialogInterface
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.performReplacingTransaction
import com.taxigoexpress.core.presentation.utils.createDialog
import com.taxigoexpress.core.R
import com.taxigoexpress.core.presentation.activities.BaseActivity
import com.taxigoexpress.core.presentation.authentication.services.KeyStoreManager

abstract class BaseFragment : Fragment() {

    val preferencesManager by lazy { this.activity?.let { SharedPreferencesManager(it) } }

    val keyStoreManager by lazy {
        this.activity?.let {
            preferencesManager?.let { fa ->
                KeyStoreManager(
                    it,
                    fa
                )
            }
        }
    }

    abstract fun getResourceLayout(): Int

    abstract fun initView(view: View, savedInstanceState: Bundle?)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(getResourceLayout(), container, false)

    private val transitions by lazy {
        BaseActivity.Transitions(
            R.anim.left_in,
            R.anim.left_out,
            R.anim.rigth_in,
            R.anim.rigth_out
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view, savedInstanceState)
    }

    open fun replaceFragment(fragment: Fragment, @IdRes container: Int, addToBackStack: Boolean) {
        fragmentManager?.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun replaceChildFragment(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        childFragmentManager.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun replaceFragmentNoAnimation(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        fragmentManager?.performReplacingTransaction(
            container,
            fragment,
            backStackTag = if (addToBackStack) fragment::class.java.name else null,
            allowStateLoss = false
        )
    }

    open fun replaceParentFragment(
        fragment: Fragment, @IdRes container: Int,
        addToBackStack: Boolean
    ) {
        parentFragment?.fragmentManager?.performReplacingTransaction(
            container,
            fragment,
            this.transitions.onCreateEnterAnimation,
            this.transitions.onCreateExitAnimation,
            this.transitions.onBackPressedEnterAnimation,
            this.transitions.onBackPressedExitAnimation,
            if (addToBackStack) fragment::class.java.name else null,
            false
        )
    }

    open fun showAlert(message: String) {
        createDialog(
            context!!,
            message,
            getString(R.string.accept),
            isCancelable = false,
            positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
        )
    }

    open fun showAlertWithResource(message: Int) {
        createDialog(
            context!!,
            getString(message),
            getString(R.string.accept),
            isCancelable = false,
            positiveListener = DialogInterface.OnClickListener { dialog, _ -> dialog.dismiss() }
        )
    }

    open fun hideToolbar() {
        this.activity?.actionBar?.hide()
    }

    open fun showToolbar() {
        this.activity?.actionBar?.show()
    }

}