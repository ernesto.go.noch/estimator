package com.taxigoexpress.core.presentation.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.EditText


class ClearableAutoCompleteEditText : AutoCompleteTextView,
    View.OnTouchListener,
    View.OnFocusChangeListener,
    TextWatcherAdapter.TextWatcherListener {

    private var loc = Location.RIGHT

    private var xD: Drawable? = null
    private val listener: Listener? = null

    private val l: OnTouchListener? = null
    private val f: OnFocusChangeListener? = null

    constructor(context: Context)
            : super(context) {
        initView()
    }

    constructor(
        context: Context,
        attributeSet: AttributeSet
    ) : super(context, attributeSet) {
        initView()
    }

    constructor(
        context: Context,
        attributeSet: AttributeSet,
        defStyle: Int
    ) : super(context, attributeSet, defStyle) {
        initView()
    }

    interface Listener {
        fun didClearText()
    }

    enum class Location(val idx: Int) {
        LEFT(0), RIGHT(2);
    }

    private fun initView() {
        super.setOnTouchListener(this)
        super.setOnFocusChangeListener(this)
        addTextChangedListener(TextWatcherAdapter(this, this))
        initIcon()
        setClearIconVisible(false)
    }

    private fun setClearIconVisible(visible: Boolean) {
        val cd = compoundDrawables
        val displayed = getDisplayedDrawable()
        val wasVisible = displayed != null
        if (visible != wasVisible) {
            val x = if (visible) xD else null
            super.setCompoundDrawables(
                if (loc == Location.LEFT) x else cd[0],
                cd[1],
                if (loc == Location.RIGHT) x else cd[2],
                cd[3]
            )
        }
    }

    override fun setCompoundDrawables(
        left: Drawable?,
        top: Drawable?,
        right: Drawable?,
        bottom: Drawable?
    ) {
        super.setCompoundDrawables(left, top, right, bottom)
        loc = Location.RIGHT
        initIcon()
    }

    private fun getDisplayedDrawable(): Drawable? =
        compoundDrawables[loc.idx]


    private fun initIcon() {
        xD = null
        xD = compoundDrawables[loc.idx]
        if (xD == null) {
            xD = resources.getDrawable(android.R.drawable.presence_offline, context.theme)
        }
        xD?.setBounds(0, 0, xD!!.intrinsicWidth, xD!!.intrinsicHeight)
        val min = paddingTop + xD!!.intrinsicHeight + paddingBottom
        if (suggestedMinimumHeight < min) {
            minimumHeight = min
        }
    }

    override fun onTextChanged(view: EditText, text: String) {
        if (isFocused) {
            setClearIconVisible(text.isNotEmpty())
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        val x = event!!.x.toInt()
        val y = event.y.toInt()
        val left =
            if (loc == Location.LEFT) 0 else width - paddingRight - xD!!.intrinsicWidth
        val right =
            if (loc == Location.LEFT) paddingLeft + xD!!.intrinsicWidth else width
        val tappedX =
            x in left..right && y >= 0 && y <= bottom - top
        if (tappedX) {
            if (event.action == MotionEvent.ACTION_UP) {
                setText("")
                listener?.didClearText()
            }
            return true
        }
        return l?.onTouch(v, event) ?: false
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (hasFocus) {
            setClearIconVisible(text.isNotEmpty())
        }
        f?.let {
            f.onFocusChange(v, hasFocus)
        }
    }
}