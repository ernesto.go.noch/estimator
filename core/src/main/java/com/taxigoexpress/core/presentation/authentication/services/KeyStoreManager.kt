package com.taxigoexpress.core.presentation.authentication.services

import android.annotation.TargetApi
import android.app.KeyguardManager
import android.content.Context
import android.os.Build
import android.provider.Settings
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.decrypt
import com.ia.mchaveza.kotlin_library.encrypt
import com.taxigoexpress.core.presentation.authentication.encryption.CipherWrapper
import com.taxigoexpress.core.presentation.authentication.encryption.KeyStoreWrapper


class KeyStoreManager(private val context: Context, private val preferencesManager: SharedPreferencesManager) {

    /**
     * The place to keep all constants.
     */
    companion object {
        val DEFAULT_KEY_STORE_NAME = "default_keystore"

        val MASTER_KEY = "MASTER_KEY"
        val CONFIRM_CREDENTIALS_KEY = "CONFIRM_CREDENTIALS_KEY"

        val CONFIRM_CREDENTIALS_VALIDATION_DELAY = 120 // Seconds
    }

    private val keyStoreWrapper = KeyStoreWrapper(context, DEFAULT_KEY_STORE_NAME)

    /**
     * Using keystore to store passwords
     */

    fun saveSensitiveData(key: String, value: String) {
        if (isDeviceScreenLocked()) {
            createMasterKey(value)
            createConfirmCredentialsKey()
            val safeData = encrypt(value, key)
            preferencesManager.setSharedPreference(key, safeData)
        } else {
            saveDataWithSafeMode(key, value)
        }
    }

    fun retrieveSensitiveData(key: String): String = if (isDeviceScreenLocked()) {
        val savedData = preferencesManager.getSharedPreference(key, "")
        decrypt(savedData, key)
    } else {
        retrieveDataWithSafeMode(key)
    }

    /**
     * Using compatibility for other sensitive data
     */

    fun saveDataWithSafeMode(key: String, value: String) {
        val encryptedData = value.encrypt()
        preferencesManager.setSharedPreference(key, encryptedData)
    }

    fun retrieveDataWithSafeMode(key: String): String =
        preferencesManager.getSharedPreference(key, "").decrypt()

    fun clearData(key: String) {
        preferencesManager.clearPreferences(key)
    }

    /**
     * Create and save cryptography key, to protect Secrets with.
     */
    private fun createMasterKey(password: String? = null) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            createAndroidSymmetricKey()
        } else {
            createDefaultSymmetricKey(password ?: "")
        }
    }

    /**
     * Remove master cryptography key. May be used for re sign up functionality.
     */
    private fun removeMasterKey() {
        keyStoreWrapper.removeAndroidKeyStoreKey(MASTER_KEY)
    }

    /**
     * Encrypt user password and Secrets with created master key.
     */
    private fun encrypt(data: String, keyPassword: String? = null): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            encryptWithAndroidSymmetricKey(data)
        } else {
            encryptWithDefaultSymmetricKey(data, keyPassword ?: "")
        }
    }

    /**
     * Decrypt user password and Secrets with created master key.
     */
    private fun decrypt(data: String, keyPassword: String? = null): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            decryptWithAndroidSymmetricKey(data)
        } else {
            decryptWithDefaultSymmetricKey(data, keyPassword ?: "")
        }
    }

    private fun createAndroidSymmetricKey() {
        keyStoreWrapper.createAndroidKeyStoreSymmetricKey(MASTER_KEY)
    }

    private fun encryptWithAndroidSymmetricKey(data: String): String {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).encrypt(data, masterKey, true)
    }

    private fun decryptWithAndroidSymmetricKey(data: String): String {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).decrypt(data, masterKey, true)
    }

    private fun createDefaultSymmetricKey(password: String) {
        keyStoreWrapper.createDefaultKeyStoreSymmetricKey(MASTER_KEY, password)
    }

    private fun encryptWithDefaultSymmetricKey(data: String, keyPassword: String): String {
        val masterKey = keyStoreWrapper.getDefaultKeyStoreSymmetricKey(MASTER_KEY, keyPassword)
        return if (masterKey != null) {
            CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).encrypt(data, masterKey, true)
        } else {
            data.encrypt()
        }
    }

    private fun decryptWithDefaultSymmetricKey(data: String, keyPassword: String): String {
        val masterKey = keyStoreWrapper.getDefaultKeyStoreSymmetricKey(MASTER_KEY, keyPassword)
        return if (masterKey != null) {
            CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).decrypt(data, masterKey, true)
        } else {
            preferencesManager.getSharedPreference(keyPassword, "").decrypt()
        }
    }

    /**
     * Create and save cryptography key, that will be used for confirm credentials authentication.
     */
    private fun createConfirmCredentialsKey() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyStoreWrapper.createAndroidKeyStoreSymmetricKey(
                CONFIRM_CREDENTIALS_KEY,
                userAuthenticationRequired = true,
                userAuthenticationValidityDurationSeconds = CONFIRM_CREDENTIALS_VALIDATION_DELAY
            )
        }
    }

    private fun isDeviceScreenLocked(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            isDeviceLocked()
        } else {
            isPatternSet() || isPassOrPinSet()
        }
    }

    /**
     * @return true if pattern set, false if not (or if an issue when checking)
     */
    private fun isPatternSet(): Boolean {
        val cr = context.contentResolver
        return try {
            val lockPatternEnable = Settings.Secure.getInt(cr, Settings.Secure. LOCK_PATTERN_ENABLED)
            lockPatternEnable == 1
        } catch (e: Settings.SettingNotFoundException) {
            false
        }

    }

    /**
     * @return true if pass or pin set
     */
    private fun isPassOrPinSet(): Boolean {
        val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager //api 16+
        return keyguardManager.isKeyguardSecure
    }

    /**
     * @return true if pass or pin or pattern locks screen
     */
    @TargetApi(23)
    private fun isDeviceLocked(): Boolean {
        val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager //api 23+
        return keyguardManager.isDeviceSecure
    }

}