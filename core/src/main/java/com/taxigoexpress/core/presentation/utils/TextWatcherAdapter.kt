package com.taxigoexpress.core.presentation.utils

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class TextWatcherAdapter(
    private val view: EditText,
    private val listener: TextWatcherListener
) : TextWatcher {

    interface TextWatcherListener {
        fun onTextChanged(view: EditText, text: String)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        this.listener.onTextChanged(view, s.toString())
    }

    override fun afterTextChanged(s: Editable?) {
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }
}